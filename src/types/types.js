export const types = {

    login: '[Auth] Login',
    logout: '[Auth] Logout',

    uiSetError: '[UI] Set error',
    uiRemoveError: '[UI] Remove Error',

    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',

    notesAddNew: '[Notes] New note',
    notesActive: '[Notes] Set active note',
    notesLoad: '[Notes] Load notes',
    notesUploated: '[Notes] Update note',
    notesFileUrl: '[Notes] Update image Url',
    notesDelete: '[Notes] Delete note',
    notesLogoutCleaning: '[Notes] Logout cleaning',
}