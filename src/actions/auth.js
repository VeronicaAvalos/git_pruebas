import { types } from "../types/types";
import Swal from 'sweetalert2'
import { firebase, googleAuthProvider } from "../firebase/firebaseConfig";
import { finishLoading, startLoading } from "./ui";
import { noteLogout } from "./notes";

export const startLoginEmailPassword = (email, password) => {
    return (distpach) => {
        distpach(startLoading());
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(({ user }) => {
                distpach(finishLoading())
                distpach(
                    login(user.uid, user.displayName)
                )
            })
            .catch( e => {
                distpach(finishLoading())
                console.log(e)
                Swal.fire('Error', e.message, 'error')
            })

    }
}

export const startGoogleLogin = () => {
    return (distpach) => {
        firebase.auth().signInWithPopup(googleAuthProvider)
            .then(({ user }) => {
                distpach(login(user.uid, user.displayName))
            });
    }
}

export const login = (uid, displayName) => ({
    type: types.login,
    payload: {
        uid,
        displayName
    }


})

export const startRegisterWithEmailPasswordName = (email, password, name) => {
    return (distpach) => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async ({ user }) => {
                await user.updateProfile({ displayName: name })
                distpach(
                    login(user.uid, user.displayName)
                )
            })
            .catch( e => {
                console.log(e)
                Swal.fire('Error', e.message, 'error')
            })
            
    }

}

export const startLogout = () => {
    return async (distpach) => {
        await firebase.auth().signOut()
       distpach(logout())
       distpach(noteLogout())
    }
}

export const logout = () => ({
   type: types.logout
})