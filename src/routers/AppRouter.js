import React, { useEffect, useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Redirect
} from "react-router-dom";
import { firebase } from "../firebase/firebaseConfig";
import { JournalScreen } from '../components/journal/JournalScreen';
import { AuthRouter } from './AuthRouter';
import { useDispatch } from 'react-redux';
import { login } from '../actions/auth';
import { PublicRoute } from './PublicRoute';
import { PrivateRoute } from './PrivateRoute';
import { startLoadingNotes } from '../actions/notes';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const [checking, setChecking] = useState(true);
    const [isLogged, setIsLogged] = useState(false)

    useEffect(() => {
        firebase.auth().onAuthStateChanged(async(user) => {
            if (user?.uid) {
                dispatch(login(user.uid, user.displayName))
                setIsLogged(true)

               dispatch(startLoadingNotes(user.uid))
                
            } else {
                setIsLogged(false)
            }
            setChecking(false)
        })
    }, [dispatch, setChecking])

    if (checking) {
        return (
            <h1>
                Wait...
            </h1>
        )
    }

    return (
        <Router>
            <div>
                <Switch>
                    <PublicRoute isAuthenticate={isLogged} path="/auth" component={AuthRouter} />
                    <PrivateRoute isAuthenticate={isLogged} path="/" component={JournalScreen} />
                    <Redirect to="auth/login" />
                </Switch>
            </div>
        </Router>
    )
}
