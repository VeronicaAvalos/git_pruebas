import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";


const firebaseConfig = {
    apiKey: "AIzaSyB7qcKKzWmN5ryZFssRbZ6B0iEQv9tuMmE",
    authDomain: "react-app-curso-9ed66.firebaseapp.com",
    databaseURL: "https://react-app-curso-9ed66.firebaseio.com",
    projectId: "react-app-curso-9ed66",
    storageBucket: "react-app-curso-9ed66.appspot.com",
    messagingSenderId: "1011687145259",
    appId: "1:1011687145259:web:52d6818aefb4de7ea08f6e"
  };

  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();
  const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
  
  export {
      db,
      googleAuthProvider,
      firebase
  }