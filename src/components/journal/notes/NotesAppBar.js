import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startSaveNote, startUpload } from '../../../actions/notes';

export const NotesAppBar = () => {

    const dispatch = useDispatch();
    const { active: note } = useSelector(state => state.notes)
    const handleSaveNote = () => {
        dispatch(startSaveNote(note))
    }

    const handleUploadImage = () => {
        document.querySelector('#fileSelector').click();
    }

    const handleFileChange = (e) => {
        console.log(e.target.files)
        const file = e.target.files[0];
        if (file) {
            dispatch(startUpload(file))
        }
    }

    return (
        <div className="notes__appbar">
            <span>
                28 de agosto 2020
            </span>
            <input
                type="file"
                id="fileSelector"
                name=""
                style={{display: 'none'}}
                onChange={handleFileChange}
            />
            <div>
                <button className="btn" onClick={handleUploadImage}>
                    Picture
                </button>
                <button className="btn" onClick={handleSaveNote}>
                    Save
                </button>
            </div>
        </div>
    )
}
