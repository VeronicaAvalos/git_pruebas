import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm';
import validator from "validator";
import { useDispatch, useSelector } from 'react-redux';
import { removeError, setError } from '../../actions/ui';
import { startRegisterWithEmailPasswordName } from '../../actions/auth';

export const RegisterScreen = () => {

    const dispatch = useDispatch();
    const {msgError} = useSelector(state => state.ui)

    console.log(msgError)
    const [formValues, handleInputChange]  = useForm({
        name: 'vero',
        email: 'vero93.ar@gmail.com',
        password: '123456',
        password2: '123456'
    });

    const { name, email, password, password2} = formValues;

    const handleRegister = (e) => {
        e.preventDefault();
       
        if(isFormValid()){
            console.log("formulario correcto")
            dispatch(startRegisterWithEmailPasswordName(email, password, name))
        }
    }

    const isFormValid = () => {
        
        if ( name.length === 0 ){
            dispatch(setError('nombre requerido'));
            return false
        }
         else if (!validator.isEmail(email)) {
            dispatch(setError('Email is not valid'));
            return false
        } else if( password !== password2 || password.length < 5 ){
            dispatch(setError('password incorrecto'));
            return false
        }
        dispatch(removeError());
        return true;
    }

    return (
        <Fragment>
            <h3 className="auth__title">Register</h3>
            <form onSubmit={handleRegister}>
                { msgError && <div className="auth__alert-error">
                    {msgError}
                </div>}
                <input
                    type="text"
                    placeholder="name"
                    name="name"
                    className="auth__input"
                    autoComplete="off"
                    value={name}
                    onChange={handleInputChange}
                />
                <input
                    type="email"
                    placeholder="email"
                    name="email"
                    className="auth__input"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChange}
                />
                <input
                    type="password"
                    placeholder="password"
                    name="password"
                    className="auth__input"
                    value={password}
                    onChange={handleInputChange}
                />

                <input
                    type="password"
                    placeholder="confirm"
                    name="password2"
                    className="auth__input"
                    value={password2}
                    onChange={handleInputChange}
                />

                <button
                    type="submit"
                    className="btn btn-primary btn-block mb-5"
                >
                    Register
                </button>



                <Link to="/auth/login" className="link mt-5">
                    Already register
                </Link>
            </form>

        </Fragment>
    )
}
